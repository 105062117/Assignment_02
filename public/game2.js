var gameStatep2 = {

    preload : function() {},
    create : function() {

        this.keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'a': Phaser.Keyboard.A,
            'd': Phaser.Keyboard.D
        });

        game.add.sprite(0, 0, 'bg');
        
        this.jumpSound = game.add.audio('jump');
        this.hitSound = game.add.audio('hit');
        this.dieSound = game.add.audio('die');

        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();
        
        this.status = 'running';
        this.distance = 0;
        this.lastTime = 0;
        this.platforms = [];
    },

    update : function() {
        if(this.status == 'gameOver' && this.keyboard.enter.isDown) this.restart();
        if(this.status != 'running') return;

        game.physics.arcade.collide([this.player1, this.player2], this.platforms, this.effect, null, this);
        game.physics.arcade.collide([this.player1, this.player2], [this.leftWall, this.rightWall]);
        game.physics.arcade.collide(this.player1, this.player2);

        this.checkTouchCeiling(this.player1);
        this.checkTouchCeiling(this.player2);
        this.checkGameOver();

        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();

        this.createPlatforms();
    },

    createBounders : function() {
        this.leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;

        this.rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

        this.ceiling = game.add.image(0, 0, 'ceiling');
    },

    createPlatforms : function() {
        if(game.time.now > this.lastTime + 600) {
            this.lastTime = game.time.now;
            this.createOnePlatform();
            this.distance += 1;
        }
    },

    createOnePlatform : function() {

        var platform;
        var x = Math.random()*(400 - 96 - 40) + 20;
        var y = 400;
        var rand = Math.random() * 100;

        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        this.platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    createPlayer : function() {
        this.player1 = game.add.sprite(300, 50, 'player1');
        game.physics.arcade.enable(this.player1);
        this.player1.body.gravity.y = 500;
        this.player1.animations.add('left', [0, 1, 2, 3], 8);
        this.player1.animations.add('right', [9, 10, 11, 12], 8);
        this.player1.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player1.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player1.animations.add('fly', [36, 37, 38, 39], 12);
        this.player1.life = 10;
        this.player1.unbeatableTime = 0;
        this.player1.touchOn = undefined;

        this.player2 = game.add.sprite(100, 50, 'player2');
        game.physics.arcade.enable(this.player2);
        this.player2.body.gravity.y = 500;
        this.player2.animations.add('left', [0, 1, 2, 3], 8);
        this.player2.animations.add('right', [9, 10, 11, 12], 8);
        this.player2.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player2.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player2.animations.add('fly', [36, 37, 38, 39], 12);
        this.player2.life = 10;
        this.player2.unbeatableTime = 0;
        this.player2.touchOn = undefined;
    },

    createTextsBoard : function() {
        var style = {fill: '#bfbfbf', fontSize: '20px', font: 'Georgia'}
        this.text1 = game.add.text(20, 15, '', style);
        this.text2 = game.add.text(20, 35, '', style);

        this.text3 = game.add.text(345, 10, '', style);
        this.text4 = game.add.text(game.width/2, game.height/2, '', style);
        this.text4.visible = false;
    },

    updatePlayer : function() {
        if(this.keyboard.left.isDown) {
            this.player1.body.velocity.x = -250;
        } else if(this.keyboard.right.isDown) {
            this.player1.body.velocity.x = 250;
        } else {
            this.player1.body.velocity.x = 0;
        }
        if(this.keyboard.a.isDown) {
            this.player2.body.velocity.x = -250;
        } else if(this.keyboard.d.isDown) {
            this.player2.body.velocity.x = 250;
        } else {
            this.player2.body.velocity.x = 0;
        }

        this.setPlayerAnimate(this.player1);
        this.setPlayerAnimate(this.player2);
    },

    setPlayerAnimate : function(player) {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;

        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
            player.frame = 8;
        }
    },

    updatePlatforms : function() {
        for(var i=0; i<this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },

    updateTextsBoard : function() {
        this.text1.setText('P1 life:' + this.player1.life);
        this.text2.setText('P2 life:' + this.player2.life);
        this.text3.setText('B' + this.distance);
    },

    effect : function(player, platform) {
        if(platform.key == 'conveyorRight') {
            this.conveyorRightEffect(player, platform);
        }
        if(platform.key == 'conveyorLeft') {
            this.conveyorLeftEffect(player, platform);
        }
        if(platform.key == 'trampoline') {
            this.trampolineEffect(player, platform);
        }
        if(platform.key == 'nails') {
            this.nailsEffect(player, platform);
        }
        if(platform.key == 'fake') {
            this.fakeEffect(player, platform);
        }
        this.basicEffect(player, platform);
    },

    conveyorRightEffect : function(player, platform) {
        player.body.x += 2;
    },

    conveyorLeftEffect : function(player, platform) {
        player.body.x -= 2;
    },

    trampolineEffect : function(player, platform) {
        platform.animations.play('jump');
        player.body.velocity.y = -350;
        this.jumpSound.play();
        this.jumpSound.volume = 0.5;
    },

    nailsEffect : function(player, platform) {
        if (player.touchOn !== platform) {
            player.life -= 3;
            player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            this.hitSound.play();
            this.hitSound.volume = 0.6;
        }
    },

    basicEffect : function(player, platform) {
        if (player.touchOn !== platform) {
            if(player.life < 10) {
                player.life += 1;
            }
            player.touchOn = platform;
            
            if(platform.key != 'trampoline')
            {
                this.hitSound.play();
                this.hitSound.volume = 0.6;
            }
        }
    },

    fakeEffect : function(player, platform) {
        if(player.touchOn !== platform) {
            if(player.life < 10) {
                player.life += 1;
            }
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;

            this.hitSound.play();
            this.hitSound.volume = 0.6;
        }
    },

    checkTouchCeiling : function(player) {
        if(player.body.y < 0) {
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime) {
                player.life -= 3;
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    checkGameOver : function() {
        if(this.player1.life <= 0 || this.player1.body.y > 500) {
            this.gameOver('player2');
        }
        if(this.player2.life <= 0 || this.player2.body.y > 500) {
            this.gameOver('player1');
        }
    },

    gameOver : function(winner) {
        this.text4.visible = true;
        this.text4.setText('勝利者 ' + winner + '\nEnter 回到主選單');
        this.text4.anchor.setTo(0.5, 0.5);

        this.platforms.forEach(function(s) {s.destroy()});
        this.platforms = [];
        
        this.dieSound.play();
        this.dieSound.volume = 0.6;
        this.dieSound.fadeOut(1000);

        this.status = 'gameOver';
        game.camera.shake(0.02, 300);
    },

    restart : function() {
        game.state.start('menu');
    }
}