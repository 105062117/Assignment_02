var gameStatep1 = {

    preload : function() {},
    create : function() {

        this.keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT
        });

        game.add.sprite(0, 0, 'bg');
        
        this.jumpSound = game.add.audio('jump');
        this.hitSound = game.add.audio('hit');
        this.dieSound = game.add.audio('die');

        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();
        
        this.status = 'running';
        this.distance = 0;
        this.lastTime = 0;
        this.platforms = [];
    },

    update : function() {
        // bad
        if(this.status == 'gameOver' && this.keyboard.enter.isDown) this.restart();
        if(this.status != 'running') return;

        game.physics.arcade.collide(this.player, this.platforms, this.effect, null, this);
        game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
        this.checkTouchCeiling();
        this.checkGameOver();

        this.updatePlayer();
        this.updatePlatforms();
        this.updateTextsBoard();

        this.createPlatforms();
    },

    createBounders : function() {
        this.leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;

        this.rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

        this.ceiling = game.add.image(0, 0, 'ceiling');
    },

    createPlatforms : function() {
        if(game.time.now > this.lastTime + 600) {
            this.lastTime = game.time.now;
            this.createOnePlatform();
            this.distance += 1;
        }
    },

    createOnePlatform : function() {

        var platform;
        var x = Math.random()*(400 - 96 - 40) + 20;
        var y = 400;
        var rand = Math.random() * 100;

        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        this.platforms.push(platform);

        if(game.global.hard)
            platform.body.velocity.x = game.rnd.integerInRange(-200, 200);
            
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    createPlayer : function() {
        this.player = game.add.sprite(200, 50, 'player1');
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player.life = 10;
        this.player.unbeatableTime = 0;
        this.player.touchOn = undefined;
    },

    createTextsBoard : function() {
        var style = {fill: '#bfbfbf', fontSize: '20px', font: 'Georgia'}
        this.text1 = game.add.text(20, 10, '', style);
        this.text2 = game.add.text(345, 10, '', style);
        this.text3 = game.add.text(game.width/2, game.height/2, 'Enter 回到主選單', style);
        
        this.text3.anchor.setTo(0.5, 0.5);
        this.text3.visible = false;
    },

    updatePlayer : function() {
        if(this.keyboard.left.isDown) {
            this.player.body.velocity.x = -250;
        } else if(this.keyboard.right.isDown) {
            this.player.body.velocity.x = 250;
        } else {
            this.player.body.velocity.x = 0;
        }
        this.setPlayerAnimate();
    },

    setPlayerAnimate : function() {
        var x = this.player.body.velocity.x;
        var y = this.player.body.velocity.y;

        if (x < 0 && y > 0) {
            this.player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            this.player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            this.player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            this.player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            this.player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
            this.player.frame = 8;
        }
    },

    updatePlatforms : function() {
        for(var i=0; i<this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 2;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
    },

    updateTextsBoard : function() {
        this.text1.setText('life:' + this.player.life);
        this.text2.setText('B' + this.distance);
    },

    effect : function(player, platform) {
        if(platform.key == 'conveyorRight') {
            this.conveyorRightEffect(player, platform);
        }
        if(platform.key == 'conveyorLeft') {
            this.conveyorLeftEffect(player, platform);
        }
        if(platform.key == 'trampoline') {
            this.trampolineEffect(player, platform);
        }
        if(platform.key == 'nails') {
            this.nailsEffect(player, platform);
        }
        if(platform.key == 'fake') {
            this.fakeEffect(player, platform);
        }
        this.basicEffect(player, platform);
    },

    conveyorRightEffect : function(player, platform) {
        this.player.body.x += 2;
    },

    conveyorLeftEffect : function(player, platform) {
        this.player.body.x -= 2;
    },

    trampolineEffect : function(player, platform) {
        platform.animations.play('jump');
        this.player.body.velocity.y = -350;
        this.jumpSound.play();
        this.jumpSound.volume = 0.5;
    },

    nailsEffect : function(player, platform) {
        if (this.player.touchOn !== platform) {
            this.player.life -= 3;
            this.player.touchOn = platform;
            game.camera.flash(0xff0000, 100);
            this.hitSound.play();
            this.hitSound.volume = 0.6;
        }
    },

    basicEffect : function(player, platform) {
        if (this.player.touchOn !== platform) {
            if(this.player.life < 10) {
                this.player.life += 1;
            }
            this.player.touchOn = platform;
            
            if(platform.key != 'trampoline')
            {
                this.hitSound.play();
                this.hitSound.volume = 0.6;
            }
        }
    },

    fakeEffect : function(player, platform) {
        if(this.player.touchOn !== platform) {
            if(this.player.life < 10) {
                this.player.life += 1;
            }
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            this.player.touchOn = platform;

            this.hitSound.play();
            this.hitSound.volume = 0.6;
        }
    },

    checkTouchCeiling : function() {
        if(this.player.body.y < 0) {
            if(this.player.body.velocity.y < 0) {
                this.player.body.velocity.y = 0;
            }
            if(game.time.now > this.player.unbeatableTime) {
                this.player.life -= 3;
                game.camera.flash(0xff0000, 100);
                this.player.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    checkGameOver : function() {
        if(this.player.life <= 0 || this.player.body.y > 500) {
            this.gameOver();
        }
    },

    gameOver : function() {
        this.text3.visible = true;
        this.platforms.forEach(function(s) {s.destroy()});
        this.platforms = [];
        
        this.dieSound.play();
        this.dieSound.volume = 0.6;
        this.dieSound.fadeOut(1000);

        this.status = 'gameOver';

        game.camera.shake(0.02, 300);
        game.time.events.add(
            500, function() {
                this.addInput();
            },this);
    },

    addInput : function(){
        var input = document.createElement('input');
            
        input.type = 'text';
        input.style.position = 'fixed';
        input.style.left = (115)+ 'px';
        input.style.top = (game.height/2+20)+ 'px';
        input.style.fontSize = 13+'px';
        input.style.border = '2px solid #000000';

        document.body.appendChild(input);
        input.focus();

        input.addEventListener("keydown", function(e){
            if (e.keyCode === 13){ 
                document.body.removeChild(this);

                var data = {
                    name: this.value,
                    score: gameStatep1.distance
                }

                var postRef = firebase.database().ref();
                postRef.push(data);
            }
        });
    },

    restart : function() {
        game.state.start('menu');
    }
}