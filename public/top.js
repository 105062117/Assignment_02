var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');

game.global = {hard: true};

game.state.add('menu', menuState);
game.state.add('gamep1', gameStatep1);
game.state.add('gamep2', gameStatep2);

game.state.start('menu');