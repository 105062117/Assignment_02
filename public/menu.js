var menuState ={
    preload : function() {

        game.load.spritesheet('player1', 'assets/player.png', 32, 32);
        game.load.spritesheet('player2', 'assets/player2.png', 32, 32);

        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('menu', 'assets/menu.png');
        game.load.image('bg', 'assets/bg.png');

        game.load.audio('jump', 'assets/sounds/jump.ogg');
        game.load.audio('hit', 'assets/sounds/hit.wav');
        game.load.audio('die', 'assets/sounds/die.wav');
        game.load.audio('bgm', 'assets/sounds/bgm.wav');
    },
    create : function(){
        this.keyboard = game.input.keyboard.addKeys({
            'one': Phaser.Keyboard.ONE,
            'two': Phaser.Keyboard.TWO,
            'three': Phaser.Keyboard.THREE
            // 'quit': Phaser.Keyboard.Q,
        });

        game.add.sprite(0, 0, 'menu');

        var style = {fill: '#1e85ae', fontSize: '20px', font: 'Georgia'}
        
        game.add.text(game.width/2, 30, 'NS-Shaft', {fill: '#1e85ae', fontSize: '30px', font: 'Georgia'}).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 100, 'Leader Board', {fill: '#1e85ae', fontSize: '25px', font: 'Georgia'}).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 140, 'Player & Score', style).anchor.setTo(0.5, 0.5);
        
        this.labelp1 = game.add.text(game.width/2, 340, 'press 1 for one player', style);
        this.labelp1.anchor.setTo(0.5, 0.5);

        this.labelp2 = game.add.text(game.width/2, 360, 'press 2 for two player', style);
        this.labelp2.anchor.setTo(0.5, 0.5);

        this.labelhard = game.add.text(game.width/2, 380, 'press 3 for nightmare mood', style);
        this.labelhard.anchor.setTo(0.5, 0.5);
        
        game.add.tween(this.labelp1).to({alpha : 0}, 1200).yoyo(true).loop().start();
        game.add.tween(this.labelp2).to({alpha : 0}, 1200).yoyo(true).loop().start();
        game.add.tween(this.labelhard).to({alpha : 0}, 1200).yoyo(true).loop().start();
        //this.labelquit = game.add.text(100, 340, 'press Q to end game', style);
        
        this.keyboard.one.onDown.add(this.p1, this);
        this.keyboard.two.onDown.add(this.p2, this);
        this.keyboard.three.onDown.add(this.hard, this);
        // this.keyboard.quit.onDown.add(this.end, this);

        this.bgmSound = game.add.audio('bgm');
        this.bgmSound.play();
        this.bgmSound.volume = 0.5;
        this.bgmSound.loop=true;

        this.loaddata(style);

    },
    update : function(){},

    p1: function() {
        this.bgmSound.stop();
        game.global.hard = false;
        game.state.start('gamep1');
    },
    p2: function() {
        this.bgmSound.stop();
        game.state.start('gamep2');
    },
    hard: function() {
        this.bgmSound.stop();
        game.global.hard = true;
        game.state.start('gamep1');
    },
    // end: function() {
    //     window.opener = null;
    //     window.close();
    // }
    loaddata : function(setting){
        var postsRef = firebase.database().ref().orderByChild("score").limitToLast(5);

        var num = [];
        var user = [];
        var place = 1;

        postsRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                num.push(childSnapshot.val().score);
                user.push(childSnapshot.val().name);
            });
          }).then(function(){
            for(var i=4; i>=0; i--){
                if(num[i])
                {
                    game.add.text(120, 250-i*20, 'N0.'+place+'     '+user[i]+'     '+num[i], setting).anchor.setTo(0, 0.5);
                    place++;
                }
            }
        });
    }
}