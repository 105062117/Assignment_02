# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


1. 設計第一個state為menu state，而preload也在此state完成，同時在這裡顯示出計分榜的資料，並會列舉出資料庫內的前五名的使用者名稱跟分數。
2. 在這裡使用tween控制alpha來製造文字閃爍的效果，且加入背景圖片以及背景音樂，並且使用鍵盤數字的1，2，3來選擇遊戲模式。
3. 遊戲共分三種模式，一般單人模式，雙人模式，以及單人困難模式。
4. 單人模式，平台除了一般的平台，還有左移右移平台，尖刺平台，彈簧平台，跟假平台，使用左右方向鍵控制，在跳到平台上跟彈簧都有音效，死掉之後還有慘叫聲，在玩家死掉之後會要求玩家輸入玩家名稱，按enter便能回到menu繼續遊玩。
5. 單人困難跟單人的差別是，所有平台都有一個x方向的移動速度，且移動方向跟速度都是不固定的，會產生種可跳平台減少的感受，其餘功能與一般單人相同。單人模式中，判斷是簡單或是困難模式的方法是用global variable控制。
6. 雙人模式，使用方向鍵控制1p，A D控制2p，兩個角色間有碰撞的判定，在死掉過後會顯示出是哪位玩家獲勝，因為有兩位玩家，所以雙人並不會把數據結果加進資料庫儲存。